package org.stepdefinition;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import com.google.common.io.Files;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class TestNgSteps {
	public static WebDriver driver;
	@Given("open the website\"https:\\/\\/demowebshop.tricentis.com\\/\"")
	public void open_the_website_https_demowebshop_tricentis_com() {
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		driver.get("https://demowebshop.tricentis.com/");
	}

	@When("Click the Login link")
	public void click_the_login_link() {
		driver.findElement(By.linkText("Log in")).click();
	}

	@When("Enter the email {string} in the Email field")
	public void enter_the_email_in_the_email_field(String string) {
		driver.findElement(By.xpath("(//input[@id=\"Email\"])")).sendKeys(string);
	}

	@When("Enter the password {string} in Password field")
	public void enter_the_password_in_password_field(String string) {
		driver.findElement(By.xpath("(//input[@id=\"Password\"])")).sendKeys(string);
	}

	@When("Click the login button")
	public void click_the_login_button() {
		driver.findElement(By.xpath("(//input[@type=\"submit\"])[2]")).click();
	}

	@Given("Click the Books icon from the categories")
	public void click_the_books_icon_from_the_categories() {
		driver.findElement(By.partialLinkText("Books")).click();
	}

	@Given("Sort the books from price High to low")
	public void sort_the_books_from_price_high_to_low() {
		WebElement sort = driver.findElement(By.id("products-orderby"));
	Select s= new Select(sort);
	s.selectByVisibleText("Price: High to Low");
	
	}
	

	@When("select two books")
	public void select_two_books() {
		driver.findElement(By.linkText("Fiction")).click();
		driver.findElement(By.id("add-to-cart-button-45")).click();
		driver.navigate().back();
		driver.findElement(By.linkText("Computing and Internet")).click();
		
	}

	@When("Add the products to cart")
	public void add_the_products_to_cart() {
		driver.findElement(By.id("add-to-cart-button-13")).click();
	}

	@Given("select Electronics icon from the categories")
	public void select_electronics_icon_from_the_categories() {
		driver.findElement(By.partialLinkText("Electronics")).click();
	}

	@Given("Select cellphones from Electronics")
	public void select_cellphones_from_electronics() {
		driver.findElement(By.partialLinkText("Cell phones")).click();
		driver.findElement(By.linkText("Smartphone")).click();
		
	}

	@When("Add  product into Cart")
	public void add_product_into_cart() {
		driver.findElement(By.id("add-to-cart-button-43")).click();
	}

	@Then("display the Count of items added to the cart.")
	public void display_the_count_of_items_added_to_the_cart() {
		Actions a= new Actions(driver);
		WebElement cart = driver.findElement(By.linkText("Shopping cart"));
		a.moveToElement(cart).build().perform();
		List<WebElement> list = driver.findElements(By.linkText("Shopping cart"));
		int size = list.size();
		System.out.println("count of cart="+size);
	}

	@Given("select Gift cards icon from the categories")
	public void select_gift_cards_icon_from_the_categories() {
		driver.findElement(By.partialLinkText("Gift Cards")).click();
	}

	@Given("Select {string} in the display")
	public void select_in_the_display(String string) {
		WebElement display = driver.findElement(By.id("products-pagesize"));
		Select s= new Select(display);
		s.selectByVisibleText("4");
	}

	@Then("Capture the name and price of one of the Gift cards displayed")
	public void capture_the_name_and_price_of_one_of_the_gift_cards_displayed() throws IOException {
		driver.findElement(By.linkText("$5 Virtual Gift Card")).click();
		TakesScreenshot tk=(TakesScreenshot)driver;
		File source = tk.getScreenshotAs(OutputType.FILE);
		File dest= new File("/home/reshma/eclipse-refresh/SeleniumAutomation/DEMOWEB/src/test/resources/screenshot/reshma1.png");
		Files.copy(source, dest);
	}

	@Given("Click the LogOut link")
	public void click_the_log_out_link() {
		driver.findElement(By.linkText("Log out")).click();
	}

	@Given("Check whether the Login icon is displayed or not")
	public void check_whether_the_login_icon_is_displayed_or_not() throws IOException {
		
		WebElement login = driver.findElement(By.linkText("Log in"));
		if (login.isDisplayed()) {
			TakesScreenshot tk=(TakesScreenshot)driver;
			File source = tk.getScreenshotAs(OutputType.FILE);
			File dest= new File("/home/reshma/eclipse-refresh/SeleniumAutomation/DEMOWEB/src/test/resources/screenshot/reshma2.png");
			Files.copy(source, dest);
			
		}
		
	}
}
