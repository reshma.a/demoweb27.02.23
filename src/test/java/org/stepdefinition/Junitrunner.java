package org.stepdefinition;

import org.junit.runner.RunWith;


import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/resources/features/testng.feature",glue = {"org.stepdifinition"},monochrome = true)
public class Junitrunner  {

}
